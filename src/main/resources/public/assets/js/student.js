var StudentLogin =  React.createClass({
	getInitialState: function() {
        return {id: ""};
      },	
      handleIdChange(event) {
  	    this.setState({id:event.target.value});
  	    }, 
      
  	 render:function(){
  		 return(<div>
			<div className="container-fluid">
    		<div className="row">
    			<div className="col-md-4">
					<h3> Student Login </h3>
					<form>
						<div className="form-group">
							<label form="login">Insert id</label>
							<input type="text" className="form-control" id="login" onChange={this.handleIdChange}/>
						</div>
							<div className="form-group">
 							<label form="loginpass">Insert Password</label>
 							<input type="password" className="form-control" id="loginpass"/>
 						</div>
					</form>
						<br/>
						<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/profile/"+this.state.id}`}>Login</Link></button> &nbsp; &nbsp; &nbsp; &nbsp;
						<button type="button" className="btn btn-outline-secondary"><Link to={`${"/"}`}>Back to menu</Link></button> 
  				</div>	
				</div>
				</div>
				</div>
  				
  		)
  	 }
	});


var StList = React.createClass({
	 getInitialState: function() {
	        return {sts: []};
	      },
	      
	    componentDidMount: function() {
	    	    $.ajax({
	    	      url: "/students",
	    	      dataType: 'json',
	    	      cache: false,
	    	      success: function(response) {
	    	        this.setState({sts: response});
	    	      }.bind(this),
	    	      error: function(xhr, status, err) {
	    	        console.error(this.props.url, status, err.toString());
	    	      }.bind(this)
	    	    });
	    	  
	      },
	      
	render:function(){
				var data = this.state.sts.map((st)=>{	
					return(
							<tr key = {st.id}>
			            	<td> {st.name} </td>
			            	<td> {st.id} </td>
			            	<td> {st.email} </td>
			            	<td> {st.address} </td>
			            	<td> <img src={st.photo} className="img-rounded" height="40px" width="40px"/> </td>
			            	
			            </tr>
				)
					})
					
		return(
		<div className="container-fluid">
        	<div className="row">
        		<div className="col-lg-12">
        			<table className="table">
        				<thead>
        					<tr>
        						<th> Name </th>
        						<th> Number </th>
        						<th> Email </th>
        						<th> Address </th>
        						<th> Photo </th>
        					</tr>
        				</thead>
        				<tbody>
                         {data}
                        </tbody>
                    </table>
                    <button type="submit" className="btn btn-primary">Submit</button>
                    <button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/profile/" + this.props.params.id}`}>Go back to profile</Link></button>
                 </div>
             </div>
         </div>	
		);
		}
	});
	
	var StudentDetails = React.createClass({
		
		 getInitialState: function() {
		        return {student: []};
		        
		      },
		      
		componentDidMount: function() {
    	    $.ajax({
    	      url: "/students",
    	      dataType: 'json',
    	      cache: false,
    	      success: function(response) {
    	        this.setState({student: response[this.props.params.id - 1] });
    	      }.bind(this),
    	      error: function(xhr, status, err) {
    	        console.error(url, status, err.toString());
    	      }.bind(this)
    	    });
    	  
      },
      
		render:function(){
			console.log(this.state.student);
				return(		     
			
				<div className="container-fluid">
	        		<div className="row">
	        			<div className="col-md-6">
	        				<div className="panel panel-default">
	        					<div className="panel-heading">
	        						<h4> {this.state.student.name} </h4>    	        				
	        					</div>
	        					<div className="panel-body">
	        						<div className="col-md-2"> 
	        							<img src={this.state.student.photo} className="img-rounded" height="150px" width="100px"/>
	        						</div>
	        						<div className="col-md-7 col-md-offset-1"> 
	        							<b> Number: </b>{this.state.student.id} <br/>
	        							<b>Email: </b>{this.state.student.email} <br/>
	        							<b>Address: </b> {this.state.student.address} 
	        						</div>
	        						<div className="col-md-7 col-md-offset-1">
        							<b> On going:</b> ajax faz o update do estado demasiado tarde para render puder aceder ao Set Ongoing, dando sempre undefined <br/>

	        	        			</div>
	        	        		</div>
	        	        		<div className="panel-footer">
	        	        			<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/edit/"+this.state.student.id+"/"+this.state.student.name+"/"+this.state.student.email+"/"+this.state.student.address}`}>Edit Personal information</Link></button>
	        					</div>
	        				</div>
	        			</div>
					</div>
				</div>
		)
		}
			});
	
	var EditSt = React.createClass({
		getInitialState: function() {
	        return {id: this.props.params.id,
	        		name: this.props.params.name,
	        		email:this.props.params.email,
	        		address:this.props.params.address,
	        		photo: this.props.params.photo,
	        		data: []};
	         },
	      
	    handleNameChange(event) {
	    	this.setState({name:event.target.value});
	    	 },
	    	    
	    handleEmailChange(event) {
		    this.setState({email: event.target.value});
	    	 },
	    	    
	    handleAddressChange(event) {
			this.setState({address: event.target.value});
	    	 },
	    	 
	    handlePhotoChange(event) {
    	    this.setState({photo: event.target.value});
    	    },
    	    
		render:function(){
			return(
					<div>
						<div className="container-fluid">
			        		<div className="row">
			        			<div className="col-md-6">
			        				<h3> Edit Student </h3>
									<form>
										<div className="form-group">
											<label form="editName">Name</label>
											<input type="text" className="form-control" id="editName" value={this.state.name} onChange={this.handleNameChange}/>
										</div>
										<div className="form-group">
											<label form="editEmail">Email</label>
											<input type="email" className="form-control" id="editEmail" value={this.state.email} onChange={this.handleEmailChange}/>
										</div>
										<div className="form-group">
										<label form="editEmail">Add email</label>
										<input type="email" className="form-control" id="editEmail"/>
										<button type="submit" className="btn btn-primary">Add new email</button>
										</div>
										<div className="form-group">
											<label form="editAddress">Address</label>
											<input type="text" className="form-control" id="editAddress" value={this.state.address} onChange={this.handleAddressChange}/>
										</div>
											<b>Photo</b>
											<input type="file" value={this.state.photo} onChange={this.handlePhotoChange}/>
											<br/>
											<button type="button" className="btn btn-outline-secondary" onClick={this.handleSubmit}><Link to={`${"/students/profile/" + this.state.id}`}>Submit</Link></button>
									</form>
									<br/>
									<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/profile/" + this.state.id}`}>Go back to profile</Link></button>
								</div>	
								</div>
								</div>
								</div>
			)
			
			},
			
			handleSubmit(e) {
				e.preventDefault();
				var newStudent = {
						name: this.state.name,
						email: this.state.email,
						id: this.state.id,
						photo: this.state.photo,
						address: this.state.address
				};
				console.log(newStudent);
				this.submit(newStudent);
			},
			
			submit(newStudent) {
			    $.ajax({
				      url: "/students",
				      type: "POST",
				      data: JSON.stringify(newStudent),
				      contentType: 'application/json',
				      dataType: 'json',
				      cache: false,
				      success: function(data) {
				    	  	newStudent.id = data;
							this.editStudent(newStudent);
							this.refs.nameInput.focus();
				      }.bind(this),
				      error: function(xhr, status, err) {
				        console.error(this.props.url, status, err.toString());
				      }.bind(this)
				    });
			},
			
			  editStudent(newStudent) {
				  this.setState(data[this.state.id]: newStudent);
			  }	});
	
	var ProfessorLogin =  React.createClass({
		getInitialState: function() {
	        return {id: ""};
	      },	
	      handleIdChange(event) {
	  	    this.setState({id:event.target.value});
	  	    }, 
	      
	  	  render:function(){
	   		 return(<div>
	 			<div className="container-fluid">
	     		<div className="row">
	     			<div className="col-md-4">
	 					<h3> Professor Login </h3>
	 					<form>
	 						<div className="form-group">
	 							<label form="login">Insert Id</label>
	 							<input type="text" className="form-control" id="login" onChange={this.handleIdChange}/>
	 						</div>
	 						<div className="form-group">
	 							<label form="loginpass">Insert Password</label>
	 							<input type="password" className="form-control" id="loginpass"/>
	 						</div>
	 					</form>
	 						<br/>
	   			
	 						<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/profile/"+this.state.id}`}>Login</Link></button>
	   				</div>	
	 				</div>
	 				</div>
	 				</div>
	   				
	   		)
	   	 }
		});
	
	
	var CoursesList = React.createClass({
		 getInitialState: function() {
		        return {courses: []};
		      },
		      
		    componentDidMount: function() {
		    	    $.ajax({
		    	      url: "/courses",
		    	      dataType: 'json',
		    	      cache: false,
		    	      success: function(response) {
		    	        this.setState({courses: response});
		    	      }.bind(this),
		    	      error: function(xhr, status, err) {
		    	        console.error(url, status, err.toString());
		    	      }.bind(this)
		    	    });
		    	  
		      },
		      
		render:function(){
			var data = this.state.courses.map((cs)=>{	
				return(
						<tr key = {cs.id}>
		            		<td> {cs.name} </td>
		            		<td> {cs.professorName} </td>
		            		<td> {cs.credits} </td>
		            		<td> {cs.evaluations[0]} </td>
		            	</tr>
			)
				})
				
		return(
		<div className="container-fluid">
	    	<div className="row">
	    		<div className="col-md-6">
	    			<table className="table">
	    				<thead>
	    					<tr>
	    						<th> Name </th>
	    						<th> Professor </th>
	    						<th> ECTS </th>
	    						<th> Next Evaluation </th>
	    					</tr>
	    				</thead>
	    				<tbody>
	                     {data}
	                    </tbody>
	                </table>
	             </div>
	         </div>
	     </div>	
		);
		}
	});
		
		var ProfessorDetails = React.createClass({
			
			 getInitialState: function() {
			        return {professor: []};
			      },
			      
			componentDidMount: function() {
	    	    $.ajax({
	    	      url: "/professors",
	    	      dataType: 'json',
	    	      cache: false,
	    	      success: function(response) {
	    	        this.setState({professor: response[this.props.params.id - 1]});
	    	      }.bind(this),
	    	      error: function(xhr, status, err) {
	    	        console.error(url, status, err.toString());
	    	      }.bind(this)
	    	    });
	    	  
	      },
	      
			render:function(){
				
			if (this.state.professor.attribute === 'Principal'){
				return(
						<div className="container-fluid">
			        		<div className="row">
			        			<div className="col-md-6">
			        				<div className="panel panel-default">
			        					<div className="panel-heading">
			        						<h4> {this.state.professor.name} </h4>    	        				
			        					</div>
			        					<div className="panel-body">
			        						<div className="col-md-2"> 
			        							<img src={this.state.professor.photo} className="img-rounded" height="150px" width="100px"/>
			        						</div>
			        						<div className="col-md-7 col-md-offset-1"> 
			        							<b>Attribute: </b>{this.state.professor.attribute} <br/>
			        							<b>Email: </b>{this.state.professor.email} <br/>
			        							<b>Department: </b> {this.state.professor.department} <br/>
			        							<b>Course: </b> {this.state.professor.course}
			        						</div>
			        	        		</div>
			        	        		<div className="panel-footer">
			        	        			<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/edit/"+this.state.professor.id+"/"+this.state.professor.name+"/"+this.state.professor.email+"/"+this.state.professor.address+"/"+this.state.professor.attribute+"/"+this.state.professor.department}`}>Edit Personal information</Link></button> &nbsp;&nbsp;
			        	        			<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/list/"+this.state.professor.id}`}>See Students List</Link></button> &nbsp;&nbsp;
			        	        			<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/courseList/"+this.state.professor.id+"/"+this.state.professor.courseId}`}>See {this.state.professor.course} Students List</Link></button> &nbsp;&nbsp;
			        	        			<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/evaluation/"+this.state.professor.id+"/"+this.state.professor.courseId}`}>Add Evaluation Step</Link></button>&nbsp;&nbsp;	
			        	        			<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/enroll/"+this.state.professor.id+"/"+this.state.professor.courseId}`}>Enroll Student to Course</Link></button>	
			        	        		</div>
			        	        	</div>
			        	        </div>
			        	    </div>
			        	</div>
			)}
					else return (
							<div className="container-fluid">
				        		<div className="row">
				        			<div className="col-md-6">
				        				<div className="panel panel-default">
				        					<div className="panel-heading">
				        						<h4> {this.state.professor.name} </h4>    	        				
				        					</div>
				        					<div className="panel-body">
				        						<div className="col-md-2"> 
				        							<img src={this.state.professor.photo} className="img-rounded" height="150px" width="100px"/>
				        						</div>
				        						<div className="col-md-7 col-md-offset-1"> 
				        							<b>Attribute: </b>{this.state.professor.attribute} <br/>
				        							<b>Email: </b>{this.state.professor.email} <br/>
				        							<b>Department: </b> {this.state.professor.department}  <br/>
				        							<b>Course: </b> {this.state.professor.course}
				        						</div>
				        	        		</div>
				        	        		<div className="panel-footer">
				    							<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/list/"+this.state.professor.id}`}>See Students List</Link></button>
				        	        			<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/courseList/"+this.state.professor.id+"/"+this.state.professor.courseId}`}>See {this.state.professor.course} Students List</Link></button> &nbsp;&nbsp;

				    							</div>
				        				</div>
				        			</div>
								</div>
							</div>)
			}
				});
		
		var EditPr = React.createClass({
			getInitialState: function() {
		        return {id: this.props.params.id,
		        		name: this.props.params.name,
		        		email:this.props.params.email,
		        		photo: this.props.params.photo,
		        		attribute: this.props.params.attribute,
		        		department: this.props.params.department,
		        		data: []};
		        },
		      
		        componentDidMount: function() {
		    	    $.ajax({
		    	      url: "/professors",
		    	      dataType: 'json',
		    	      cache: false,
		    	      success: function(data) {
		    	        this.setState({data: data});
		    	      }.bind(this),
		    	      error: function(xhr, status, err) {
		    	        console.error(this.props.url, status, err.toString());
		    	      }.bind(this)
		    	    });
		    	  },
		    	  
		      handleNameChange(event) {
		    	    this.setState({name:event.target.value});
		    	    },
		    	    
		     handleEmailChange(event) {
		    	    this.setState({email: event.target.value});
		   	    	},
		   	    	
	    	 handlePhotoChange(event) {
		    	    this.setState({photo: event.target.value});
		    	    },
		    	    
			render:function(){
				return(
						<div>
						<div className="container-fluid">
			        		<div className="row">
			        			<div className="col-md-6">
			        				<h3> Edit Professor </h3>
									<form>
										<div className="form-group">
											<label form="editName">Name</label>
											<input type="text" className="form-control" id="editName" value={this.state.name} onChange={this.handleNameChange}/>
										</div>
										<div className="form-group">
											<label form="editEmail">Email</label>
											<input type="email" className="form-control" id="editEmail" value={this.state.email} onChange={this.handleEmailChange}/>
										</div>
											<b>Photo</b>
											<input type="file" value={this.state.photo} onChange={this.handlePhotoChange}/>
											<br/>
											<button type="button" className="btn btn-outline-secondary" onClick={this.handleSubmit}><Link to={`${"/students/professors/profile/" + this.state.id}`}>Submit</Link></button>
									</form>
									<br/>
									<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/profile/" + this.state.id}`}>Go back to profile</Link></button>
								</div>	
								</div>
								</div>
								</div>
				)
				
				},
				
				handleSubmit(e) {
					e.preventDefault();
					var newProf = {
							name: this.state.name,
							email: this.state.email,
							id: this.state.id,
							photo: this.state.photo,
							attribute: this.state.attribute,
							department: this.state.department
					};
					console.log(newProf);
					this.submit(newProf);
				},
				
				submit(newProf) {
				    $.ajax({
					      url: "/professors",
					      type: "POST",
					      data: JSON.stringify(newProf),
					      contentType: 'application/json',
					      dataType: 'json',
					      cache: false,
					      success: function(data) {
					    	  	newProf.id = data;
								this.editProf(newProf);
								this.refs.nameInput.focus();
					      }.bind(this),
					      error: function(xhr, status, err) {
					        console.error(this.props.url, status, err.toString());
					      }.bind(this)
					    });
				},
				
				  editProf(newProf) {
					  this.setState(data[this.state.id]: newProf);
				  }	
			});
	
		var addEvaluation = React.createClass({
			
		getInitialState:function(){
			return{evaluation:'', courses:[]}
		},
		
		componentDidMount: function() {
    	    $.ajax({
    	      url: "/courses",
    	      dataType: 'json',
    	      cache: false,
    	      success: function(response) {
    	        this.setState({courses: response});
    	      }.bind(this),
    	      error: function(xhr, status, err) {
    	        console.error(url, status, err.toString());
    	      }.bind(this)
    	    });
    	  
      },
			render:function(){
				
				console.log(this.state.courses[this.props.params.courseId-1]);

			return(<div>
			<div className="container-fluid">
        		<div className="row">
        			<div className="col-md-6">
						<h3> Add Evaluation Step:  </h3>
						<form>
							<div className="form-group">
								<label form="addStep">Name</label>
								<input type="text" className="form-control" id="addStep"  value={this.state.evaluation} onChange={this.handleEvaluationChange}/>
							</div>
						</form>
							<br/>
							<button type="button" className="btn btn-outline-secondary" onClick={this.handleSubmit}><Link to={`${"/students/professors/profile/" + this.props.params.id}`}>Submit</Link></button><br/>
							<br/>
							<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/profile/" + this.props.params.id}`}>Go back to profile</Link></button>
						</div>
						</div>
						</div>
						</div>)
			},
			
			handleEvaluationChange(event) {
	    	    this.setState({evaluation: event.target.value});
	   	    	},
							
			handleSubmit(e) {
				e.preventDefault();
				var newEvaluation = {
						name: this.state.courses[this.props.params.courseId-1].name,
						id: this.state.courses[this.props.params.courseId-1].id,
						professor: this.state.courses[this.props.params.courseId-1].professor,
						credits: this.state.courses[this.props.params.courseId-1].credits,
						evaluation: this.state.courses[this.props.params.courseId-1].evaluations.concat(this.state.evaluation)
				};
				this.submit(newEvaluation);
				this.setState(() => ({evaluation:''}));
			},
			
			submit(newEvaluation) {
			    $.ajax({
				      url: "/courses",
				      type: "POST",
				      data: JSON.stringify(newEvaluation),
				      contentType: 'application/json',
				      dataType: 'json',
				      cache: false,
				      success: function(data) {
				    	  newEvaluation.id = data;
						this.appendEvaluation(newEvaluation);
						this.refs.nameInput.focus();
				      }.bind(this),
				      error: function(xhr, status, err) {
				        console.error(this.props.url, status, err.toString());
				      }.bind(this)
				    });
			},
			  appendEvaluation(newEvaluation) {
				this.setState(courses[this.props.params.courseId-1]: newEvaluation);
			  }			
			
		});
		
		var register =  React.createClass({
			getInitialState: function() {		
				return {name: '', email: '', address: '', data:[]};
			},
			
		    componentDidMount: function() {
	    	    $.ajax({
	    	      url: "/students",
	    	      dataType: 'json',
	    	      cache: false,
	    	      success: function(response) {
	    	        this.setState({data: response});
	    	      }.bind(this),
	    	      error: function(xhr, status, err) {
	    	        console.error(this.props.url, status, err.toString());
	    	      }.bind(this)
	    	    });
	    	  
	      },

	      handleChangeName(e) {
				this.setState({name: e.target.value});
			},
			
			handleChangeEmail(e) {
				this.setState({email: e.target.value});
			},
			
			handleChangeAddress(e) {
				this.setState({address: e.target.value});
			},
			
			render:function(){
				return(
						<div>
						<div className="container-fluid">
			        		<div className="row">
			        			<div className="col-md-6">
			        				<h3> Register Student </h3>
									<form>
										<div className="form-group">
											<label form="name">Name</label>
											<input type="text" className="form-control" id="name"  onChange={this.handleChangeName} value={this.state.name}/>
										</div>
										<div className="form-group">
											<label form="email">Email</label>
											<input type="email" className="form-control" id="email"  onChange={this.handleChangeEmail} value={this.state.email}/>
										</div>
											<div className="form-group">
				 							<label form="loginpass">Password</label>
				 							<input type="password" className="form-control" id="loginpass"/>
				 						</div>
										<div className="form-group">
											<label form="address">Address</label>
											<input type="text" className="form-control" id="address"  onChange={this.handleChangeAddress} value={this.state.address}/>
										</div>
											<b>Photo</b>
											<input type="file" />
											<br/>
											<button type="submit" className="btn btn-outline-secondary" onClick={this.handleSubmit}><Link to={`${"/students"}`}>Submit</Link></button>
									</form>
									<br/>
									<button type="button" className="btn btn-outline-secondary"><Link to={`${"/"}`}>Back to menu</Link></button>
								</div>	
								</div>
								</div>
								</div>
				)
				
				},
									

				handleSubmit(e) {
					e.preventDefault();
					var newStudent = {
							name: this.state.name,
							email: this.state.email,
							address: this.state.address,
							id: this.props.number_of_students + 1,
					};
					this.submit(newStudent);
					this.setState(() => ({name: '', email:'', address:''}));
					alert("Student Registered");
				},
				
				submit(student) {
				    $.ajax({
					      url: "/students",
					      type: "POST",
					      data: JSON.stringify(student),
					      contentType: 'application/json',
					      dataType: 'json',
					      cache: false,
					      success: function(data) {
					        student.id = data;
							this.appendStudent(student);
							this.refs.nameInput.focus();
					      }.bind(this),
					      error: function(xhr, status, err) {
					        console.error(this.props.url, status, err.toString());
					      }.bind(this)
					    });
				},
				  appendStudent(student) {
					  this.setState((prevstate) => ({ data: prevstate.data.concat(student) }));
				  }
		});
		
		var FrontPage = React.createClass ({
		
			getInitialState: function() {
			    return {data: []};
			  },
			  
			componentDidMount: function() {
			    $.ajax({
			      url: "/degrees",
			      dataType: 'json',
			      cache: false,
			      success: function(data) {
			        this.setState({data: data});
			      }.bind(this),
			      error: function(xhr, status, err) {
			        console.error(this.props.url, status, err.toString());
			      }.bind(this)
			    });
			  },
		
			
			  render: function(){
				  
					var deg = this.state.data.map((cs)=>{	
						return(
								<tr key = {cs.id}>
				            		<td> {cs.name} </td>
				            	</tr>
					)
						})
				  	return (
				  			<div>
				  				<div className="container-fluid">
				  					<div className="row">
				  						<div className="col-md-4 col-md-offset-4">
				  							<img src="http://tele1.dee.fct.unl.pt/st_2015_2016/figs/logo_quadrado%20JMF.gif" height="200px" width="300px"/>
				  				    	</div> <br/>
				  						<div className="col-md-5 col-md-offset-3">
				  				    		<table className="table">
				  				    			<thead>
				  				    				<tr>
				  				   						<th> All college courses </th>
				  				   					</tr>
				  				   				</thead>
				  				   				<tbody>
				  			                     {deg}
				  			                    </tbody>
				  			                </table>
				  			             </div>
				  			             <div className="col-md-6 col-md-offset-3">
						  						<button type="button" className="btn btn-outline-success"><Link to={`${"/students"}`}>Student Login</Link></button>
						  						&nbsp;&nbsp;
						  						<button type="button" className="btn btn-outline-success"><Link to={`${"/students/register"}`}>Register New Student</Link></button>
						  						&nbsp;&nbsp;
						  						<button type="button" className="btn btn-outline-danger"><Link to={`${"/students/professors"}`}>Professor Login</Link></button>
						  						&nbsp;&nbsp;
						  						<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/add/"}`}>Admin functions</Link></button>
						  				 </div>
						  			</div>
						  		</div>
						  	</div>)
		}
		});
		
		var AddStudent = React.createClass({
			getInitialState: function() {		
				return {name: '', email: '', address: '', data:[]};
			},
			
		    componentDidMount: function() {
	    	    $.ajax({
	    	      url: "/students",
	    	      dataType: 'json',
	    	      cache: false,
	    	      success: function(response) {
	    	        this.setState({data: response});
	    	      }.bind(this),
	    	      error: function(xhr, status, err) {
	    	        console.error(this.props.url, status, err.toString());
	    	      }.bind(this)
	    	    });
	    	  
	      },

			render: function() {
				return (
					<div>
						<h3>Add a new student</h3>
						<form onSubmit={this.handleSubmit}>
							<p>
							<label htmlFor="students_name">Name:</label>
							<input id="students_name" ref="nameInput" onChange={this.handleChangeName} value={this.state.name} />
							<span> </span>
							<label htmlFor="students_email">Email:</label>
							<input id="students_email" onChange={this.handleChangeEmail} value={this.state.email} />
							<span> </span>
							<label htmlFor="students_address">Address:</label>
							<input id="students_address" onChange={this.handleChangeAddress} value={this.state.address} />
							<span> </span>
							<button>{'Add ' + this.state.name }</button>
							</p>
						</form>
					</div>
				);
			},

			handleChangeName(e) {
				this.setState({name: e.target.value});
			},
			
			handleChangeEmail(e) {
				this.setState({email: e.target.value});
			},
			
			handleChangeAddress(e) {
				this.setState({address: e.target.value});
			},

			handleSubmit(e) {
				e.preventDefault();
				var newStudent = {
						name: this.state.name,
						email: this.state.email,
						address: this.state.address,
						id: this.props.number_of_students + 1,
				};
				this.submit(newStudent);
				this.setState(() => ({name: '', email:'', address:''}));
			},
			
			submit(student) {
			    $.ajax({
				      url: "/students",
				      type: "POST",
				      data: JSON.stringify(student),
				      contentType: 'application/json',
				      dataType: 'json',
				      cache: false,
				      success: function(data) {
				        student.id = data;
						this.appendStudent(student);
						this.refs.nameInput.focus();
				      }.bind(this),
				      error: function(xhr, status, err) {
				        console.error(this.props.url, status, err.toString());
				      }.bind(this)
				    });
			},
			  appendStudent(student) {
				  this.setState((prevstate) => ({ data: prevstate.data.concat(student) }));
			  }
			});
		
		var AddProfessor = React.createClass({
			getInitialState: function() {		
				return {attribute: '', name: '', email: '',  data:[]};
			},
			
		    componentDidMount: function() {
	    	    $.ajax({
	    	      url: "/professors",
	    	      dataType: 'json',
	    	      cache: false,
	    	      success: function(response) {
	    	        this.setState({data: response});
	    	      }.bind(this),
	    	      error: function(xhr, status, err) {
	    	        console.error(this.props.url, status, err.toString());
	    	      }.bind(this)
	    	    });
	    	  
	      },

			render: function() {
				return (
					<div>
						<h3>Add a new professor</h3>
						<form onSubmit={this.handleSubmit}>
							<p>
							<label htmlFor="professors_attribute">Attribute:</label>
							<input id="professors_attribute" onChange={this.handleChangeAttribute} value={this.state.attribute} />
							<span> </span>
							<label htmlFor="professors_name">Name:</label>
							<input id="professors_name" ref="nameInput" onChange={this.handleChangeName} value={this.state.name} />
							<span> </span>
							<label htmlFor="professors_email">Email:</label>
							<input id="professors_email" onChange={this.handleChangeEmail} value={this.state.email} />
							<span> </span>
							
							<button>{'Add ' + this.state.name }</button>
							</p>
						</form>
					</div>
				);
			},
			
			handleChangeAttribute(e) {
				this.setState({attribute: e.target.value});
			},

			handleChangeName(e) {
				this.setState({name: e.target.value});
			},
			
			handleChangeEmail(e) {
				this.setState({email: e.target.value});
			},
			

			handleSubmit(e) {
				e.preventDefault();
				var newProfessor = {
						attribute: this.state.attribute,
						name: this.state.name,
						email: this.state.email,
						id: this.props.number_of_professors + 1,
				};
				this.submit(newProfessor);
				this.setState(() => ({attribute: '', name: '', email:''}));
			},
			
			submit(professor) {
			    $.ajax({
				      url: "/professors",
				      type: "POST",
				      data: JSON.stringify(professor),
				      contentType: 'application/json',
				      dataType: 'json',
				      cache: false,
				      success: function(data) {
				        professor.id = data;
						this.appendProfessor(professor);
						this.refs.nameInput.focus();
				      }.bind(this),
				      error: function(xhr, status, err) {
				        console.error(this.props.url, status, err.toString());
				      }.bind(this)
				    });
			},
			  appendProfessor(professor) {
				  this.setState((prevstate) => ({ data: prevstate.data.concat(professor) }));
			  }
			});
		
		var AddUsers =  React.createClass({
		
		  	 render:function(){
		  		 return(<div>
					<div className="container-fluid">
		    			<div className="row">
		    				<div className="col-md-10">
		    				<h1> Add Users </h1>
		    					<AddStudent/>
		    					<AddProfessor/>
		    				</div>
		    				<div className="col-md-4">
		    					<button type="button" className="btn btn-outline-secondary"><Link to={`${"/"}`}>Go back</Link></button>
		    				</div>
						</div>
						</div>
						</div>
		  				
		  		)
		  	 }
			});
	
		var EnrollStudent = React.createClass({
			getInitialState: function() {		
				return {studentid: ''};
			},
			
			 handleIdChange(event) {
		  	    this.setState({studentid:event.target.value});
		  	    }, 
	      
	      render:function(){
	    	  return(<div>
	    		<div className="container-fluid">
	        		<div className="row">
	        			<div className="col-md-6">
							<h3> Enroll Student to Course  </h3>
							<form>
								<div className="form-group">
									<label form="snumber">Student Number:</label>
									<input type="text" className="form-control" value={this.state.studentid} onChange={this.handleIdChange}/>
								</div>
							</form>
							<br/>
							<button type="button" className="btn btn-outline-secondary" onClick={this.handleSubmit}><Link to={`${"/students/professors/profile/" + this.props.params.id}`}>Enroll</Link></button><br/>
							<br/>
							<button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/profile/" + this.props.params.id}`}>Go back to profile</Link></button>
						</div>
					</div>
				</div>
	    	</div>
	    	  )
	      },
	      
	      handleSubmit(e) {
	    	  e.preventDefault();
	    	  var url1 = "/courses/enrollstudent/";
			    var url2 = this.props.params.courseId.toString();
			    var url3= this.state.studentid.toString();
			    var url4= "/";
			    var finalUrl = url1.concat(url2.concat(url4.concat(url3)));
			    	$.ajax({
			    	      url: finalUrl  ,
			    	      dataType: 'json',
			    	      cache: false,
			    	      success: function(response) {
			    	       
			    	      }.bind(this),
			    	      error: function(xhr, status, err) {
			    	        console.error(this.props.url, status, err.toString());
			    	      }.bind(this)
			    	    });
	      }
	      
		});
		
		var StListbyCourse = React.createClass({
			 getInitialState: function() {
			        return {sts: []};
			      },
			      
			    componentDidMount: function() {
			    var url1 = "/students/coursesearch/";
			    var url2 = this.props.params.courseId.toString();
			    var finalUrl = url1.concat(url2);
			    	$.ajax({
			    	      url: finalUrl  ,
			    	      dataType: 'json',
			    	      cache: false,
			    	      success: function(response) {
			    	        this.setState({sts: response});
			    	      }.bind(this),
			    	      error: function(xhr, status, err) {
			    	        console.error(this.props.url, status, err.toString());
			    	      }.bind(this)
			    	    });
			    	  
			      },
			      
			render:function(){
						var data = this.state.sts.map((st)=>{	
							return(
									<tr key = {st.id}>
					            	<td> {st.name} </td>
					            	<td> {st.id} </td>
					            	<td> {st.email} </td>
					            	<td> {st.address} </td>
					            	<td> <img src={st.photo} className="img-rounded" height="40px" width="40px"/> </td>
					            	<td> <select>
					            	<option value="0">0</option>
					            	<option value="1">1</option>
					            	<option value="2">2</option>
					            	<option value="3">3</option>
					            	<option value="4">4</option>
					            	<option value="5">5</option>
					            	<option value="6">6</option>
					            	<option value="7">7</option>
					            	<option value="8">8</option>
					            	<option value="9">9</option>
					            	<option value="10">10</option>
					            	<option value="11">11</option>
					            	<option value="12">12</option>
					            	<option value="13">13</option>
					            	<option value="14">14</option>
					            	<option value="15">15</option>
					            	<option value="16">16</option>
					            	<option value="17">17</option>
					            	<option value="18">18</option>
					            	<option value="19">19</option>
					            	<option value="20">20</option>
					            	</select></td>
					            </tr>
						)
							})
							
				return(
				<div className="container-fluid">
		        	<div className="row">
		        		<div className="col-lg-12">
		        			<table className="table">
		        				<thead>
		        					<tr>
		        						<th> Name </th>
		        						<th> Number </th>
		        						<th> Email </th>
		        						<th> Address </th>
		        						<th> Photo </th>
		        						<th> Mark </th>
		        					</tr>
		        				</thead>
		        				<tbody>
		                         {data}
		                        </tbody>
		                    </table>
		                    <button type="submit" className="btn btn-primary">Submit</button>
		                    <button type="button" className="btn btn-outline-secondary"><Link to={`${"/students/professors/profile/" + this.props.params.id}`}>Go back to profile</Link></button>
		                 </div>
		             </div>
		         </div>	
				);
				}
			});
		
var Link = ReactRouter.Link;
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
	
	ReactDOM.render((
		<Router>
    		<Route path="/" component={FrontPage}/>
	    	<Route path="/students" component={StudentLogin}/>
	    	<Route path="/students/add" component={AddUsers}/>
	    	<Route path="/students/profile/:id" component={StudentDetails}/>
	    	<Route path="/students/courses/" component={CoursesList}/>
	    	<Route path="/students/edit/:id/:name/:email/:address" component={EditSt}/>
	    	<Route path="/students/register" component={register}/>
	    	<Route path="/students/professors" component={ProfessorLogin}/>
	    	<Route path="/students/professors/list/:id" component={StList}/>
	    	<Route path="/students/professors/evaluation/:id/:courseId" component={addEvaluation}/>
	    	<Route path="/students/professors/enroll/:id/:courseId" component={EnrollStudent}/>
	    	<Route path="/students/professors/courseList/:id/:courseId" component={StListbyCourse}/>
	    	<Route path="/students/professors/profile/:id" component={ProfessorDetails}/>
	    	<Route path="/students/professors/edit/:id/:name/:email/:address/:attribute/:department" component={EditPr}/>
	    </Router>
		), document.getElementById('react_content'))