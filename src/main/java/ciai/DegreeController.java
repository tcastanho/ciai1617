package ciai;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.model.Degree;
import ciai.model.DegreeRepository;

import ciai.views.Views;

@Controller
@RequestMapping(value = "/degrees")
public class DegreeController {

	@Autowired
	DegreeRepository degrees;
	
	@RequestMapping(value = "")
	@JsonView(Views.ExtendedDegreeView.class)
	public @ResponseBody Iterable<Degree> getDegrees() {
		return degrees.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@JsonView(Views.ExtendedDegreeView.class)
	public @ResponseBody Degree getDegree(@PathVariable long id) {
		return degrees.findOne(id);
	}

	
	@RequestMapping(value= "", method=RequestMethod.POST)
	public @ResponseBody long addDegree(@RequestBody Degree degree) {
		degrees.save(degree);
		return degree.getId();
	}
}
