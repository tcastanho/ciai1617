package ciai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import ciai.model.Course;
import ciai.model.CourseRepository;
import ciai.model.Degree;
import ciai.model.DegreeRepository;
import ciai.model.Enrollment;
import ciai.model.Professor;
import ciai.model.ProfessorRepository;
import ciai.model.Student;
import ciai.model.StudentRepository;
import ciai.model.UserRepository;

@SpringBootApplication
@EnableSpringDataWebSupport
public class ClassroomApplication {

	private static final Logger log = LoggerFactory.getLogger(ClassroomApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ClassroomApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner init(
			StudentRepository repository, 
			CourseRepository courseRepository, 
			ProfessorRepository professors,
			DegreeRepository degrees,
			UserRepository users) {
		
		return (args) -> {
			Student[] students =
				{new Student("Ricardo Fernandes", "Rua de Corroios", "rja.fernandes@campus.fct.unl.pt"),
				 new Student("Tiago Castanho", "Rua do Barreiro", "t.castanho@campus.fct.unl.pt"),
				 new Student("André Catela", "Rua de Loures", "a.catela@campus.fct.unl.pt"),
				 new Student("Francisco Cardoso", "Rua de Telheiras", "fp.cardoso@campus.fct.unl.pt"),
				 new Student("Manuel Cruz", "Rua de Corroios", "md.cruz@campus.fct.unl.pt"),
				 new Student("João Santos", "Rua de Carnaxide", "j.santos@campus.fct.unl.pt")};
		
			for(Student s: students) repository.save(s);

			log.info("Students found with findAll():");
			log.info("-------------------------------");
			for (Student ss: repository.findAll()) {
				log.info(ss.toString());
			}
            log.info("");

			// fetch an individual customer by ID
			Student student = repository.findOne(1L);
			log.info("Student found with findOne(1L):");
			log.info("--------------------------------");
			log.info(student.toString());
            log.info("");

             // fetch student by name
			log.info("Student email found with findByName('Ricardo Fernandes'):");
			log.info("--------------------------------------------");
			for (Student jorjao : repository.findByName("Ricardo Fernandes")) {
				log.info(jorjao.toString());
			}
            log.info("");

			// fetch student by email
			log.info("Student found with findByEmail('rja.fernandes@campus.fct.unl.pt'):");
			log.info("--------------------------------------------");
			for (Student rja : repository.findByEmail("rja.fernandes@campus.fct.unl.pt")) {
				log.info(rja.toString());
			}
            log.info("");

            // fetch student by address
			log.info("Student address found with findByAddress('Rua de Corroios'):");
			log.info("--------------------------------------------");
			for (Student corroios : repository.findByAddress("Rua de Corroios")) {
				log.info(corroios.toString());
			}
            log.info("");
            
			// search student by name 
			log.info("Student found with search('R'):");
			log.info("--------------------------------------------");
			for (Student namedR : repository.search("R")) {
				log.info(namedR.toString());
			}
            log.info("");
            
            Course[] courses = { new Course("AED",6), 
            		             new Course("CIAI",6), 
            		             new Course("ICL",6) };

			for(Course c: courses) courseRepository.save(c);
			
			log.info("Courses found with findAll():");
			log.info("-------------------------------");
			for (Course cc: courseRepository.findAll()) {
				log.info(cc.toString());
			}
            log.info("");
			
			log.info("Updating course name 'AED' to 'AED1':");
			log.info("-------------------------------");
			Course aed = courseRepository.findByName("AED").get(0);
			aed.setName("AED1");	
			courseRepository.save(aed);
			
			log.info("Enrolling students in 'AED1':");
			log.info("-------------------------------");
			for(Student ss:repository.findAll())
			aed.getEnrollments().add(new Enrollment(aed,ss, "AED1"));			
			courseRepository.save(aed);
            log.info("");

			log.info("Enrolling students in 'ICL':");
			log.info("-------------------------------");
			Course icl = courseRepository.findByName("ICL").get(0);
			for(Student ss:repository.findAll())
			icl.getEnrollments().add(new Enrollment(icl,ss, "ICL"));			
			courseRepository.save(icl);
            log.info("");

            log.info("Courses found with findAll():");
			log.info("-------------------------------");
			for (Course cc: courseRepository.findAll()) {
				log.info(cc.toString());
				log.info("-------------------------------");
				for(Enrollment e: cc.getEnrollments())
					log.info(e.toString());
				log.info("-------------------------------");
				log.info("");
			}
            log.info("");

            log.info("Courses found with findAll():");
			log.info("-------------------------------");
			for (Student s: repository.findAll()) {
				log.info(s.toString());
				log.info("-------------------------------");
				for(Enrollment e: s.getOnGoing())
					log.info(e.getCourse().toString());
				log.info("-------------------------------");
				log.info("");
			}
            log.info("");
            
            log.info("Students found with findByCourse(id):");
			log.info("-------------------------------");
			//Course c = courseRepository.findOne(1L);
			for (Student s: repository.findByCourse(1L)) {
				log.info(s.toString());
			}
			log.info("-------------------------------");
            log.info("");    
            
            Professor profs[] = {new Professor("João Costa Seco", "Assistant", "jrcs@fct.unl.pt", "Informatics"),
            		             new Professor("Jácome Cunha", "Principal", "jacome@fct.unl.pt", "Informatics")};
            
           
            	Professor p2 = profs[0];
            	p2.setCourses(icl.getName());
            	p2.setcId(icl.getId());
            	p2.setCoursesObj(icl);
            	icl.setProfName(p2.getName());
            	professors.save(p2);
            	courseRepository.save(icl);
            	
            	Professor p1 = profs[1];
            	p1.setCourses(aed.getName());
            	p1.setcId(aed.getId());
            	p1.setCoursesObj(aed);
            	aed.setProfName(p1.getName());
            	professors.save(p1);
            	courseRepository.save(aed);
            
			log.info("Adding courses to professors");
			log.info("-------------------------------");
			
			
            
            log.info("Professors found with findByAll(id):");
			log.info("-------------------------------");
            for(Professor p: professors.findAll()) {
            	log.info(p.getName());
    			log.info("-------------------------------");
            
            }
			log.info("-------------------------------");
            log.info("");    

            Degree degr[] = {new Degree("Computer Science"), 
            					new Degree("Mathematics")};
            
            for(Degree d: degr) degrees.save(d);

            
            //BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            
           // User[] us = {new User("Joao",encoder.encode("CostaSeco")), 
            //			 new User("Jacome", encoder.encode("Cunha")) };
            
            //for(User u: us) users.save(u);

		};
	}

}
