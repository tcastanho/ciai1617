package ciai;

import java.util.List;

import javax.print.attribute.standard.PageRanges;
import javax.transaction.Transactional;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.model.Course;
import ciai.model.CourseRepository;
import ciai.model.Enrollment;
import ciai.model.Professor;
import ciai.model.ProfessorRepository;
import ciai.model.Student;
import ciai.model.StudentRepository;
import ciai.views.Views;

@Controller
@RequestMapping(value = "/professors")
public class ProfessorController {

	@Autowired
	ProfessorRepository professors;
	
	@Autowired
	CourseRepository courses;
	
	@Autowired
	StudentRepository students;
	
	@RequestMapping(value = "")
	@JsonView(Views.ProfessorView.class)
	public @ResponseBody Iterable<Professor> getProfessors(){
		return professors.findAll();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@JsonView(Views.ExtendedProfessorView.class)
	public @ResponseBody Professor getProfessor(@PathVariable long id) {
		return professors.findOne(id);
	}

	
	@RequestMapping(value= "", method=RequestMethod.POST)
	public @ResponseBody long addCourse(@RequestBody Professor prof) {
		professors.save(prof);
		return prof.getId();
	}
	
	
}
