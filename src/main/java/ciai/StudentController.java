package ciai;

import java.util.List;

import javax.print.attribute.standard.PageRanges;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.model.Student;
import ciai.model.StudentRepository;
import ciai.views.Views;

@Controller
@RequestMapping(value = "/students")
public class StudentController {

	@Autowired
	StudentRepository students;
	
	@RequestMapping(value = "")
	@JsonView(Views.StudentView.class)
	public @ResponseBody Iterable<Student> getStudents(
			@RequestParam(value = "course", required = false) String name) {
		if( name == null )
			return students.findAll();
		else
			return students.searchByCourse(name);
	}
	
	@RequestMapping(value = "/coursesearch/{id}")
	@JsonView(Views.StudentView.class)
	public @ResponseBody Iterable<Student> getStudentsCourse(@PathVariable long id) {
			return students.findByCourse(id);
	}
	
	@RequestMapping(value= "/page")
	public @ResponseBody Page<Student> getStudentsPaged(
			@RequestParam(required=false, defaultValue =  "0") Integer page, 
			@RequestParam(required=false, defaultValue = "3") Integer size) {
		return students.findAll(new PageRequest(page, size));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@JsonView(Views.ExtendedStudentView.class)
	public @ResponseBody Student getStudent(@PathVariable long id) {
		return students.findOne(id);
	}

	
	@RequestMapping(value= "", method=RequestMethod.POST)
	//@AllowedForAdmin
	public @ResponseBody long addStudent(@RequestBody Student student) {
		students.save(student);
		return student.getId();
	}
	
	
}
