package ciai.views;

public class Views {
	public interface StudentView {}
	public interface CourseView {}
	public interface DegreeView {}
	public interface ProfessorView {}
	public interface ExtendedStudentView extends StudentView, CourseView {}
	public interface ExtendedCourseView extends CourseView, StudentView {}
	public interface ExtendedDegreeView extends DegreeView {}
	public interface ExtendedProfessorView extends ProfessorView {}
}
