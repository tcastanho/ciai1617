package ciai;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.model.Course;
import ciai.model.CourseRepository;
import ciai.model.Enrollment;
import ciai.model.Student;
import ciai.model.StudentRepository;
import ciai.views.Views;

@Controller
@RequestMapping(value = "/courses")
public class CourseController {

	@Autowired
	CourseRepository courses;

	@Autowired
	StudentRepository students;
	
	@RequestMapping(value = "")
	@JsonView(Views.ExtendedCourseView.class)
	public @ResponseBody Iterable<Course> getCourses() {
		return courses.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@JsonView(Views.ExtendedCourseView.class)
	public @ResponseBody Course getCourse(@PathVariable long id) {
		return courses.findOne(id);
	}

	@RequestMapping(value = "/{id}/students", method = RequestMethod.GET)
	@JsonView(Views.StudentView.class)
	public @ResponseBody List<Student> getStudents(@PathVariable long id) {
		return students.findByCourse(id);
	}

	@RequestMapping(value = "/enrollstudent/{id}/{studentId}")
	public @ResponseBody long addStudent(@PathVariable long id, @PathVariable long studentId) {
		Course course = courses.findOne(id);
		Student student = students.findOne(studentId);
		course.getEnrollments().add(new Enrollment(course,student, course.getName()));
		courses.save(course);
		return course.getId();
	}
	
	@RequestMapping(value= "", method=RequestMethod.POST)
	public @ResponseBody long addCourse(@RequestBody Course course) {
		courses.save(course);
		return course.getId();
	}
}
