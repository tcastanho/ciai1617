package ciai.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.views.Views;

@Entity
public class Enrollment {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    @ManyToOne
    @NotNull
    @JsonView(Views.StudentView.class)
    private Course course;

    @JsonView(Views.StudentView.class)
    private String courseName;
    
    @ManyToOne
    @NotNull
    @JsonView(Views.CourseView.class)
	private Student student;
		
    public Enrollment() {}
    
    public Enrollment(Course course, Student student, String courseName) {
    	this.setCourse(course);
    	this.setStudent(student);
    	this.setCourseName(courseName);
    }
    
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public void setCourseName(String name) {
		this.courseName = name;
	}
	
	public String getCourseName() {
		return courseName;
	}
	@Override
	public String toString() {
		return "Enrollment[student = "+ student.toString()+ ", course ="+ course.toString()+"]";
	}
}
