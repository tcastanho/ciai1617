package ciai.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.views.Views;

import java.util.List;

@Entity
public class Degree{

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
    @JsonView(Views.DegreeView.class)
	private String name;
	
	@OneToMany (mappedBy="degree")
	@JsonView(Views.DegreeView.class)
	private List<Course> courseList;
	
	public Degree(String name) {
		this.setName(name);
	}
	
	public Degree(){}
	
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public List<Course> getCourseList(){
		return courseList;
	}

	public void setCourseList(List<Course> newValue){
		this.courseList=newValue;
	}
}