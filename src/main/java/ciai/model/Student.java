package ciai.model;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.views.Views;

import java.util.List;
import java.util.Set;


@Entity
public class Student {

	@JsonView(Views.StudentView.class)
	private String name;
	
	@JsonView(Views.StudentView.class)
	private String address;
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonView(Views.StudentView.class)
	private long id;
	
    @JsonView(Views.StudentView.class)
	private String email;
	
    @JsonView(Views.StudentView.class)
	private String photo;
	
	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonView(Views.StudentView.class)
	private Set<Enrollment> ongoing;
	
	@ManyToMany
	private List<Course> completed;

	public Student(String name, String address, String email) {
		this.setName(name);
		this.setAddress(address);
		this.setEmail(email);
	}
	
	public Student(){}
	
	// sets and gets
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public Set<Enrollment> getOnGoing(){
		return ongoing;
	}

	public void setOnGoing(Set<Enrollment> newValue){
		this.ongoing=newValue;
	}

	public List<Course> getCompleted() {
		return completed;
	}
	
	public void setCompleted(List<Course> newValue){
		this.completed=newValue;
	}
}
