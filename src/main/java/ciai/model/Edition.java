package ciai.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import java.util.List;

@Entity
public class Edition{

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
    
    @OneToMany
	private List<Course> courseList;
	
	public Edition(int id) {
		this.setId(id);
	}
    
	public Edition(){}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	public List<Course> getCourseList(){
			return courseList;
		}

		public void setCourseList(List<Course> newValue) {
			this.courseList = newValue;
		}
}