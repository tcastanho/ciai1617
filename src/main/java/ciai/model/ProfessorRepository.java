package ciai.model;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ProfessorRepository extends CrudRepository<Professor, Long> {

	Professor findByName(String name);
	
    List<Student> findByEmail(String email);
    

}
