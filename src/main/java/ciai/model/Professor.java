package ciai.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonView;

import ciai.views.Views;

import java.util.List;
import java.util.Set;

@Entity
public class Professor {

    @JsonView(Views.ProfessorView.class)
	private String name;
    
    @JsonView(Views.ProfessorView.class)
	private String attribute;

    @JsonView(Views.ProfessorView.class)
	private String department;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@JsonView(Views.ProfessorView.class)
	private long id;
	
    @JsonView(Views.ProfessorView.class)
	private String email;

    @JsonView(Views.ProfessorView.class)
    private String photo;
	
	
    @JsonView(Views.ProfessorView.class)
	private String course;
    
    @OneToOne
    @JsonView(Views.ProfessorView.class)
  	private Course courseObj;

	@JsonView(Views.ProfessorView.class)
	private long courseId;
	
	public Professor(String name, String attribute, String email, String department) {
		this.setName(name);
		this.setAttribute(attribute);
		this.setEmail(email);
		this.setDepartment(department);
	}
	
    public Professor() {}

	// sets and gets
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public long getcId() {
		return courseId;
	}

	public void setcId(long id) {
		this.courseId = id;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getCourses(){
		return course;
	}

	public void setCourses(String newValue) {
		this.course = newValue;
	}

	public Course getCoursesObj(){
		return courseObj;
	}

	public void setCoursesObj(Course newValue) {
		this.courseObj = newValue;
	}

}
