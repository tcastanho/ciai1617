package ciai.model;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface DegreeRepository extends CrudRepository<Degree , Long> {

    List<Degree> findByName(String name);

}