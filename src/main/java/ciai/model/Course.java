package ciai.model;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import ciai.views.Views;

import javax.persistence.ManyToMany;


import java.util.List;
import java.util.Set;


@Entity
public class Course {

    @JsonView(Views.CourseView.class)
	private String name;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonView(Views.CourseView.class)
	private long id;
    
    @OneToOne
    @JsonView(Views.CourseView.class)
	private Professor professor;
    

    @JsonView(Views.CourseView.class)
	private String professorName;
    
    @JsonView(Views.CourseView.class)
    private int credits;
    
    @JsonView(Views.CourseView.class)
    private String[] evaluations;
	
	@OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonView(Views.ExtendedCourseView.class)
	@JsonIgnore
	private Set<Enrollment> enrollments;
	
	@ManyToOne
	private Degree degree;

	public Course(String name, int credits) {
		this.setCredits(credits);
		this.setName(name);
		this.setEvaluation(new String[0]);
	
	}
	
    public Course() {}

	// sets and gets
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getCredits() {
		return credits;
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	public String getName() {
		return name;
	}

	public void setProfName(String name) {
		this.professorName = name;
	}
	
	public String getProfName() {
		return professorName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Professor getProfessors(){
		return professor;
	}

	public void setProfessors(Professor newValue) {
		this.professor = newValue;
	}
	
	public Set<Enrollment> getEnrollments() {
		return enrollments;
	}

	public void setEnrollments(Set<Enrollment> enrollments) {
		this.enrollments = enrollments;
	}
	
	public Degree getDegree(){
		return degree;
	}

	public void setDegree(Degree newValue) {
		this.degree = newValue;
	}
	
	public String[] getEvaluations(){
		return evaluations;
	}

	public void setEvaluation(String[] newValue) {
		this.evaluations = newValue;
	}
}
